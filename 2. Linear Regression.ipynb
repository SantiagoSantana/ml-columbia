{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linear Regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Regression: Problem Definition\n",
    "\n",
    "### Data\n",
    "\n",
    "**Input:** $ x \\in {\\mathbb{R}}^{d} $ (i.e., measurements, covariates, features, independent variables)  \n",
    "**Output:** $ y \\in \\mathbb{R} $ (i.e., response, dependent variable)\n",
    "\n",
    "### Goal\n",
    "Find a function $ f: \\mathbb{R}^{d} \\to \\mathbb{R} $ such that $ y \\approx f(x, w) $ for the data pair $ (x, y) $.\n",
    "$ f(x, w) $ is called a _regression function_. Its free parameters are $ w $.\n",
    "\n",
    "### Definition of Linear Regression\n",
    "A regression method is called _linear_ if the prediction $ f $ is a _linear function_ of the unknown parameters $ w $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Least Squares Linear Regression Model\n",
    "\n",
    "### Model\n",
    "\n",
    "The linear regression model we focus on now has the form\n",
    "\n",
    "$$ y_i \\approx f(x_i; w) = w_0 + \\sum_{j=1}^{d}x_{ij}w_j $$\n",
    "\n",
    "### Model Learning\n",
    "\n",
    "We have the set of _training data_ $ (x_1, y_1) \\dots (x_n, y_n) $. We want to use this data to learn a $ w $ such that $ y_i \\approx f(x_i, w) $. But we first need an _objective function_ to tell us what a \"good\" value of $ w $ is.\n",
    "\n",
    "### Least Squares\n",
    "\n",
    "The _least squares_ objective tell us to pick the $ w $ that minimizes the sum of squared errors\n",
    "\n",
    "$$ w_{LS} = \\arg \\underset{w} \\min \\sum_{i=1}^{n} (y_i - f(x_i; w))^2 \\equiv \\arg \\underset{w} \\min L $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notation\n",
    "\n",
    "\\begin{align}\n",
    "&\\hat{y_i} = f(x_i; w) \\\\\n",
    "&y_i = \\hat{y_i} + \\epsilon\n",
    "\\end{align}\n",
    "\n",
    "What we want is to minimize the objective function\n",
    "\n",
    "\\begin{equation*}\n",
    "L = \\sum_{i=1}^{n} \\epsilon_i^2 = \\sum_{i=1}^{n} (y_i - w_0 - \\sum_{j=1}^{d} x_{ij}w{j})^{2}\n",
    "\\end{equation*}\n",
    "\n",
    "with respect to $ (w_0, w_1, ..., w_d) $\n",
    "\n",
    "The term $ w_0 $ is the intercept thus does not interact with any element in the vector $ x \\in \\mathbb{R}^d $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation*}\n",
    "x_i = \\begin{bmatrix}1 \\\\ x_{i1} \\\\ x_{i2} \\\\ \\vdots \\\\ x_{id}\\end{bmatrix}\n",
    "\\text{,}\n",
    "\\boldsymbol{X} = \n",
    "\\begin{bmatrix}\n",
    "1 & x_{11} & \\dots & x_{1d} \\\\\n",
    "1 & x_{21} & \\dots & x_{2d} \\\\\n",
    "\\vdots && \\vdots \\\\\n",
    "1 & x_{n1} & \\dots & x_{nd}\n",
    "\\end{bmatrix}\n",
    "\\ =\n",
    "\\begin{bmatrix}\n",
    "1 & x_1^T \\\\\n",
    "1 & x_2^T \\\\\n",
    "\\vdots & \\vdots \\\\\n",
    "1 & x_n^T \\\\\n",
    "\\end{bmatrix}\n",
    "\\end{equation*}\n",
    "\n",
    "We also now view $ w = [w_0, w_1, \\dots, w_d]^T $ as $ w \\in \\mathbb{R}^{d+1} $\n",
    "\n",
    "$ x $ has $ d $ **features** or **dimensions** and $ n $ **observations**  \n",
    "We transpose $ x $ so that **each row corresponds to one observation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Assumptions (for now)\n",
    "\n",
    "* All functions are treated as continuous-valued $ (x \\in \\mathbb{R}^n) $\n",
    "* We have more observations than dimensions $ (d < n) $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can write our _least squares objective function_ in vector form as\n",
    "\\begin{equation*}\n",
    "L = \\sum_{i=1}^{n} (y_i - x_{i}^{T}w) ^ 2\n",
    "\\end{equation*}\n",
    "\n",
    "We can find $ w $ by setting,\n",
    "\n",
    "\\begin{equation*}\n",
    "\\nabla_{w} L = 0 \\Rightarrow w_{LS} = \\sum_{i=1}^{n} \\nabla_w(y_i^2 - 2_w^T x_i y_i + w^T x_i x_i^T w) = 0\n",
    "\\end{equation*}\n",
    "\n",
    "Solving gives\n",
    "\n",
    "\\begin{equation*}\n",
    "-\\sum_{i=1}^{n} 2y_i x_i + (\\sum_{i=1}^{n} 2 x_i x_i^T)w = 0 \\\\\n",
    "w_{LS} = (\\sum_{i=1}^{n} x_i x_i^T)^{-1} (\\sum_{i=1}^{n}y_ix_i)\n",
    "\\end{equation*}\n",
    "\n",
    "in matrix form is even cleaner\n",
    "\n",
    "\\begin{equation}\n",
    "w_{LS} = (\\boldsymbol{X}^T \\boldsymbol{X})^{-1} \\boldsymbol{X}^T y\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Polynomial Regression in $ \\mathbb{R} $\n",
    "\n",
    "A function such as $ y = w_0 + w_1 x + w_2 x^2 $ is _linear_ in $ w $, only the preprocessing of the data is different.\n",
    "\n",
    "E.g. let $ (x_1, y_1),\\dots,(x_n, y_n) $ be the data, $ x \\in \\mathbb{R} \\text{,} y \\in \\mathbb{R} $. For a pth-order polynomial approximation, construct the matrix\n",
    "\n",
    "\\begin{align}\n",
    "\\boldsymbol{X} = \n",
    "\\begin{bmatrix}\n",
    "1 & x_1 & x_1^2 & \\dots & x_1^n \\\\\n",
    "1 & x_2 & x_2^2 & \\dots & x_2^n \\\\\n",
    "\\vdots & & & \\vdots \\\\\n",
    "1 & x_n & x_n^2 & \\dots & x_n^n \\\\\n",
    "\\end{bmatrix}\n",
    "\\end{align}\n",
    "\n",
    "then solve\n",
    "\n",
    "\\begin{equation}\n",
    "w_{LS} = (\\boldsymbol{X}^T \\boldsymbol{X})^{-1} \\boldsymbol{X}^T y\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $ M $th order (in $ \\mathbb{R} $, one dimension)\n",
    "\n",
    "#### $ M = 0 $\n",
    "\n",
    "Without any data $ x $ is a vector of ones $ (w_0) $, a horizontal line in $ y = 2 $\n",
    "\n",
    "#### $ M = 1 $\n",
    "\n",
    "\\begin{equation*}\n",
    "\\boldsymbol{X} =\n",
    "\\begin{bmatrix}\n",
    "1 & x_1 \\\\\n",
    "1 & x_2 \\\\\n",
    "\\vdots & \\vdots \\\\\n",
    "1 & x_n \\\\\n",
    "\\end{bmatrix}\n",
    "\\end{equation*}\n",
    "\n",
    "#### $ M = 2 $ \n",
    "\n",
    "\\begin{equation*}\n",
    "\\boldsymbol{X} =\n",
    "\\begin{bmatrix}\n",
    "1 & x_1 & x_1^2 \\\\\n",
    "1 & x_2 & x_2^2 \\\\\n",
    "\\vdots & & \\vdots \\\\\n",
    "1 & x_n & x_n^2 \\\\\n",
    "\\end{bmatrix}\n",
    "\\end{equation*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Polynomial Regression in two or more dimensions\n",
    "\n",
    "The width of $ \\boldsymbol{X} $ grows as $ \\text{(order)} \\times \\text{(dimensions)} + 1 $\n",
    "\n",
    "**i.e.**\n",
    "\n",
    "\\begin{align}\n",
    "\\text{2nd order: } y_i &= w_0 + w_1 x_{i1} + w_2 x_{i2} + w_3 x_{i1}^2 + w_4 x_{i2}^2 \\\\\n",
    "\\text{3nd order: } y_i &= w_0 + w_1 x_{i1} + w_2 x_{i2} + w_3 x_{i1}^2 + w_4 x_{i2}^2 + w_5 x_{i1}^3 + w_6 x_{i2}^3\n",
    "\\end{align}"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
